// 1) Метод обʼєкту це функціі внутри обʼєкту.
// 2) Number, string, null , bolean , object, true/false.
// 3) Об'єкт це посилальний тип даних. Звератбчись до обʼєкт, ми посилаємось на ячейку памʼяті/*коробка* обʼєкта... 

function createNewUser( ) {
    let name= prompt ("What is your name?");
    let surname = prompt ("What is your surname?");

    return  {
        _firstName: name,
        _lastName: surname,

        getLogin: function() {
            return this._firstName.charAt(0).toLowerCase() + this._lastName.toLowerCase();
        },

        get firstName() {
            return this._firstName;
        },

        setFirstName(name) {
             this._firstName = name;
        },

        get lastName() {
            return this._lastName;
        },

        setLastName(name) {
             this._lastName = name;
        },  
    };   
}             

let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getLogin()); 
console.log("---------------------------");
console.log("----Without functions:-----")
newUser.firstName = prompt ("Change your name...")
newUser.lastName = prompt ("Change your surname...")
console.log(newUser);
console.log("------------------------");
console.log("----Whith functions:----")
newUser.setFirstName ( prompt ("Change your name..."));
newUser.setLastName ( prompt ("Change your surname..."));
console.log(newUser);
console.log("---------------------------");
console.log(newUser.getLogin());